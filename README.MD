# Tic, tac, toe


### DESCRIPTION
This is a Tic, tac, toe game with basic functionality. As I understood it should be made from scratch, so I didn't use any additional library, like Vue, or similar. I set here structure on which it could be developed further. I believe it is enough to figure out coding approach and style. Visuals are not very polished, as I understood it is not a primarily focus. I could add some library to help visuals, like Bootstrap, but than again my thinking was that coding style is more important, and speed of deliverence. Off course, no login system is included, humans should play on the same computer, and AI is not very smart currently. Game was made on Thursday 5th of November, and Friday morning of 6th of November.

# 4. ADDITION
You can inspect the the game on https://gitlab.com/k0b1/tic-tac-toe, and I also deployed it to my test server here: http://88.198.156.214:8085/
