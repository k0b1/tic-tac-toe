export default class GameState {
  moves = 0;
  history = [];

  resetState() {
    this.moves = 0;
    this.history = [];
  }

  goBack() {
    const button = document.getElementById("back-button");
    button.addEventListener("click", () => {
      if (this.history.length > 0) {
        const field = document.querySelector(
          "#" + this.history[this.moves - 1][1] + " img"
        );
        this.history.pop();
        field.parentNode.removeChild(field);
        this.moves--;
      }
    });
  }
}
