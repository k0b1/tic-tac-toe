export default class HTMLElements {
  container = document.getElementById("ttt-game__container");

  createButtonContainer() {
    const div = document.createElement("div");
    div.id = "action-buttons-container";
    div.className = "ttt__actions-container";
    return this.container.appendChild(div);
  }

  createButton(parent, text, className, idName) {
    const backButton = document.createElement("button");
    backButton.className = className; // "ttt__back-button";
    backButton.id = idName;
    backButton.textContent = text;
    return parent.appendChild(backButton);
  }

  createImage(path, className, alt) {
    const mark = document.createElement("img");
    mark.src = path;
    mark.className = className;
    mark.alt = alt;
    return mark;
  }
}
