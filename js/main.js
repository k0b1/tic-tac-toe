import HTMLElements from "./HTMLElements";
import GameState from "./GameState";

class HumanPlayer {
  constructor(name) {
    this.name = name;
  }
}

class AIPlayer {
  constructor(name) {
    this.name = name;
  }
}

class GameWorld {
  constructor(State, HumanPlayer, HTML) {
    this.gameState = new State();
    this.playerOne = new HumanPlayer("Player 1");
    this.playerTwo = new HumanPlayer("Player 2");
    this.html = new HTML();
  }

  field(id) {
    const field = document.createElement("div");
    field.className = "ttt-field";
    field.setAttribute("data-field-id", id);
    field.id = "field-" + id++;
    document.getElementById("ttt-game__container").appendChild(field);
  }

  listenFieldClick(id) {
    const field = document.getElementById(id);
    const dataId = field.getAttribute("data-field-id");
    field.addEventListener("click", () => {
      this.markField(id, dataId);
      this.claimVictory(this.playerOne.name, this.playerTwo.name);
    });
  }

  markField(fieldId, dataId) {
    let player;
    const field = document.getElementById(fieldId);
    let mark;

    if (this.gameState.moves % 2 === 0) {
      mark = this.html.createImage(
        "./assets/icons/x.svg",
        "is-x",
        "Tic tac toe field"
      );
      player = this.playerOne.name;
    } else {
      mark = this.html.createImage(
        "./assets/icons/o.svg",
        "is-o",
        "Tic tac toe field"
      );
      player = this.playerTwo.name;
    }
    this.gameState.history[this.gameState.moves] = [
      mark.src,
      fieldId,
      dataId,
      player
    ];
    this.gameState.moves++;
    field.appendChild(mark);
  }

  claimVictory(playerOne, playerTwo) {
    const mapVictory = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    let thisIsTheEnd = false;
    const resultPlayerOne = this.getResult(playerOne);
    const resultPlayerTwo = this.getResult(playerTwo);
    mapVictory.forEach(winning_stamp => {
      if (this.isPlayerWinner(resultPlayerOne, winning_stamp)) {
        alert(this.playerOne.name + " wins");
        thisIsTheEnd = true;
      }
      if (this.isPlayerWinner(resultPlayerTwo, winning_stamp)) {
        alert(this.playerTwo.name + " wins");
        thisIsTheEnd = true;
      }
    });

    if (this.gameState.history.length === 9 && !thisIsTheEnd) {
      thisIsTheEnd = true;
      alert("Nobody won.");
    }
    if (thisIsTheEnd) {
      this.shouldStartAgain();
    }
  }

  getResult(player) {
    return this.gameState.history
      .filter(item => {
        if (item[3] === player) {
          return item[2];
        }
      })
      .map(item => {
        return item[2];
      });
  }

  isPlayerWinner(marked, winning_target) {
    if (
      (marked.includes(String(winning_target[0])) &&
        marked.includes(String(winning_target[1])) &&
        marked.includes(String(winning_target[2]))) ||
      (marked.includes(+winning_target[0]) &&
        marked.includes(+winning_target[1]) &&
        marked.includes(+winning_target[2]))
    ) {
      return true;
    }
    return false;
  }

  shouldStartAgain() {
    this.gameState.history.forEach(item => {
      let resetFields = document.querySelector("#" + item[1] + " img");
      resetFields.parentNode.removeChild(resetFields);
    });
    this.gameState.resetState();
  }

  removePreviousGame() {
    document.getElementById("ttt-game__container").innerHTML = "";
    this.gameState.resetState();
  }

  init() {
    for (let i = 0; i < 9; i++) {
      this.field(i);
      this.listenFieldClick("field-" + i);
    }
    this.html.createButtonContainer();
    this.html.createButton(
      document.getElementById("action-buttons-container"),
      "Go back one step",
      "ttt_back-button ttt__button is-red",
      "back-button"
    );
    this.html.createButton(
      document.getElementById("action-buttons-container"),
      "Start again",
      "ttt_again-button ttt__button is-green",
      "again-button"
    );
    this.gameState.goBack();
    document.getElementById("again-button").addEventListener("click", () => {
      this.shouldStartAgain();
    });
  }
}

class AIGameWorld extends GameWorld {
  freeFields = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  shouldHumanPlay = true;

  constructor(State, HumanPlayer, AIPlayer, HTML) {
    super(State, HumanPlayer, HTML);
    this.playerOne = new HumanPlayer("Player 1");
    this.playerTwo = new AIPlayer("AI");
  }

  markField(fieldId, dataId) {
    if (this.shouldHumanPlay) {
      let player;
      const field = document.getElementById(fieldId);
      const mark = this.html.createImage(
        "./assets/icons/x.svg",
        "is-x",
        "Tic tac toe field"
      );
      player = this.playerOne.name;
      field.appendChild(mark);
      this.gameState.history[this.gameState.moves] = [
        mark.src,
        fieldId,
        dataId,
        player
      ];
      this.shouldHumanPlay = false;
    }
    this.gameState.moves++;
  }

  getRandomFreeNumber() {
    this.gameState.history.forEach(move => {
      if (this.freeFields.includes(Number(move[2]))) {
        delete this.freeFields[Number(move[2])];
      }
    });
    const getRandArray = this.freeFields.filter(num => {
      return num !== "empty";
    });

    const getRandFreenumber =
      getRandArray[Math.floor(Math.random() * getRandArray.length)];
    return getRandFreenumber;
  }

  aiWatch() {
    setInterval(() => {
      if (!this.shouldHumanPlay) {
        this.aiMarkField();
      }
    }, 2000);
  }

  // not smartest AI in the world, still it won me few times :/
  aiMarkField() {
    let randNum = this.getRandomFreeNumber();
    const mark = this.html.createImage(
      "./assets/icons/o.svg",
      "is-o",
      "Tic tac toe field"
    );
    const player = this.playerTwo.name;
    const getEmptyField = document.getElementById("field-" + randNum);
    getEmptyField.appendChild(mark);

    this.gameState.history[this.gameState.moves] = [
      mark.src,
      "field-" + randNum,
      randNum,
      player
    ];
    getEmptyField.click();
    this.shouldHumanPlay = true;
  }

  shouldStartAgain() {
    this.gameState.history.forEach(item => {
      let resetFields = document.querySelector("#" + item[1] + " img");
      resetFields.parentNode.removeChild(resetFields);
    });
    this.gameState.resetState();
    this.freeFields = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  }
}

document.getElementById("start-human").addEventListener("click", () => {
  const buttonHumans = document.getElementById("start-human");
  buttonHumans.className = "ttt__button is-on";
  const buttonAi = document.getElementById("start-ai");
  buttonAi.className = "ttt__button is-off";
  const start = new GameWorld(GameState, HumanPlayer, HTMLElements);
  start.removePreviousGame();
  start.init();
});

document.getElementById("start-ai").addEventListener("click", () => {
  const buttonHumans = document.getElementById("start-human");
  buttonHumans.className = "ttt__button is-off";
  const buttonAi = document.getElementById("start-ai");
  buttonAi.className = "ttt__button is-on";
  const startAI = new AIGameWorld(
    GameState,
    HumanPlayer,
    AIPlayer,
    HTMLElements
  );
  startAI.removePreviousGame();
  startAI.init();
  startAI.aiWatch();
});
